function createNewUser() {
    
    this.checkValue = function(name){
        return name !== null && name !== "" && name !== undefined && !+name
    }

    this.firstName = prompt("Введите Ваше имя");
    while(!this.checkValue(this.firstName)){
        this.firstName = prompt("Введите Ваше имя правильно");
    }
    this.lastName = prompt("Введите Вашу фамилию");
    while(!this.checkValue(this.lastName)){
        this.lastName = prompt("Введите Вашу фамилию правильно");
    }
    this.getLogin = function(){
        let login = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
           return login;
    }


}
    const newUser = new createNewUser(); 
    console.log(`Ваш логин: ${newUser.getLogin()}`);
